import plotly.graph_objects as go
import pandas as pd
import numpy as np
import openpyxl
import re
import gdown



#get data from google sheet and store it as excel sheet 
url = 'https://docs.google.com/spreadsheets/d/1BbnaOa30h8ymDkl5CL8OGLUkw4wzl2FqpdkyS2PKpps/export?format=xlsx'
output = 'output_filename.xlsx'
gdown.download(url, output, quiet=False)



# Function to extract hyperlinks from xlsx
def extract_hyperlinks_from_xlsx_full_name(file_name, column_name="Full Name", valid_indices=None):
    workbook = openpyxl.load_workbook(file_name)
    sheet = workbook.active

    links = []
    pattern = re.compile(r'\"(http[^\"]+)\"')

    column_idx = [cell.value for cell in sheet[1]].index(column_name) + 1

    # Only process rows that are in valid_indices
    for idx in valid_indices:
        cell = sheet.cell(row=idx + 2, column=column_idx)  # +2 because openpyxl is 1-indexed and sheet's first row contains column names
        if cell.hyperlink:
            links.append(cell.hyperlink.target)
        elif isinstance(cell.value, str):
            match = pattern.search(cell.value)
            if match:
                links.append(match.group(1))
            else:
                links.append(None)
        else:
            links.append(None)

    return links

# Function to extract hyperlinks from xlsx
def extract_hyperlinks_from_xlsx_implementation(file_name, column_name="Implementation(s) available?", valid_indices=None):
    workbook = openpyxl.load_workbook(file_name)
    sheet = workbook.active

    column_idx = [col[0].column for col in sheet.iter_cols() if col[0].value == column_name][0]
    links_list = []
    pattern = re.compile(r'https?://\S+')

    # Only process rows that are in valid_indices
    for idx in valid_indices:
        value = sheet.cell(row=idx + 2, column=column_idx).value
        if isinstance(value, str):
            links = pattern.findall(value)
            links_list.append(links)
        else:
            links_list.append([])

    return links_list







def random_positions_within_category(num_positions, angle_start, angle_end):
    """
    Generate random theta and r values within a given angular sector.
    """
    thetas = np.linspace(angle_start, angle_end, num=num_positions+1)[:-1] + (angle_end - angle_start)/(2*num_positions)
    
    # Generate r values that are more spread out
    rs = np.linspace(0.2, 1.1, num_positions)
    np.random.shuffle(rs)
    
    # Shuffle thetas to distribute technologies randomly within category
    np.random.shuffle(thetas)
    
    return list(thetas), list(rs)


# Read from Google sheet (or local xlsx)
file_name = "output_filename.xlsx"
df = pd.read_excel(file_name, engine="openpyxl")



# Filter out technologies missing necessary data
df = df.dropna(subset=['General Category'])


# Dynamic categories
categories = df['General Category'].unique().tolist()
n_categories = len(categories)
angle_per_category = 360 / n_categories

df['Theta'] = np.nan
df['R'] = np.nan

for category in categories:
    techs_in_category = df[df['General Category'] == category]
    angle_start = categories.index(category) * angle_per_category
    angle_end = (categories.index(category) + 1) * angle_per_category

    thetas, rs = random_positions_within_category(len(techs_in_category), angle_start, angle_end)

    # Assign the random positions to the technologies in the current category
    df.loc[df['General Category'] == category, 'Theta'] = thetas
    df.loc[df['General Category'] == category, 'R'] = rs

full_names = df['Full Name'].tolist()
acronyms = df['Acronym'].tolist()
# Extract hyperlinks
filtered_indices = df.index.tolist() # Extract indices of the filtered DataFrame
links = extract_hyperlinks_from_xlsx_full_name(file_name, valid_indices=filtered_indices) # Extract hyperlink from the Full Name
links_implementation = extract_hyperlinks_from_xlsx_implementation(file_name, valid_indices=filtered_indices) # Extract hyperlink(s) from the "Implementation(s) available?" column 
tech_thetas = df['Theta'].tolist()
tech_rs = df['R'].tolist()

fig = go.Figure()

# Boundary lines for dividing segments based on dynamic categories
for i in range(n_categories):
    angle = i * angle_per_category
    fig.add_trace(go.Scatterpolar(
        r=[0, 1.2],
        theta=[angle, angle],
        mode='lines',
        line=dict(color='grey'), #this is overwritten below 
        showlegend=False
    ))

    
    
# Plotting the technologies
for i, acronym in enumerate(acronyms):
    # Ensure that all double quotes are escaped inside the hovertext.
    description = df['Description'].iloc[i]
    if isinstance(description, str):  # Check if it's a string
        description = description.replace('"', '&quot;')
    else:
        description = "No description available."  # Or whatever default message you want
        
    links_imp = "<br>".join([f"<a href='{link_implementation}' target='_blank'>{link_implementation}</a>" for link_implementation in links_implementation[i]])
    if links_imp:
        links_imp = "Implementation link(s):<br>" + links_imp + "<br>"

    hover_text = """
        <b>{full_name}</b><br>
        <a href='{link}' target='_blank'>Link</a><br>
        {links_imp}<br>
        {description}
    """.format(full_name=full_names[i].replace('"', '&quot;'), 
               link=links[i], 
               links_imp=links_imp,
               description=description)
    
    fig.add_trace(go.Scatterpolar(
        r=[tech_rs[i]],
        theta=[tech_thetas[i]],
        mode="markers",
        marker=dict(size=10, line=dict(color='white', width=2)),  # Added line attribute here
        name=acronym,
        text=acronym,  # this will be what's displayed initially on hover
        customdata=[hover_text],  # this will be the detailed content
        hoverinfo="none"
    ))
    
    
    
#styling (Radar effect)
# Plotting concentric circles
for r in np.linspace(0.2, 1.2, 6):  # for 6 concentric circles
    fig.add_trace(go.Scatterpolar(
        r=[r]*360,  # same radius value, repeated 360 times
        theta=list(range(360)),  # angles from 0 to 359
        mode='lines',
        line=dict(color='grey', width=1),
        showlegend=False,
        hoverinfo='skip'  # suppress hover for these circle traces
    ))


# Radial lines for categories
for i in range(n_categories):
    angle = i * angle_per_category
    fig.add_trace(go.Scatterpolar(
        r=[0, 1.2],
        theta=[angle, angle],
        mode='lines',
        line=dict(color='lightblue', width=3),
        showlegend=False
    ))
#end styling

    


# Update layout
tickvals = [(i + 0.5) * angle_per_category for i in range(n_categories)]  # Placing category labels at the center of each segment

fig.update_layout(
    width=1000,  # in pixels
    height=800,  # in pixels
    title="Technology Radar",
    font=dict(family="Arial, sans-serif", size=12, color="#ADD8E6"),
    paper_bgcolor="black",  # Black background for radar effect
    plot_bgcolor="black",  # Also set the plot background to black
    polar=dict(
        bgcolor="black",  # Dark background for radar effect
        radialaxis=dict(visible=False, range=[0, 1.2]),
        angularaxis=dict(
            direction="clockwise",
            period=360,
            tickvals=tickvals,
            ticktext=categories,
            showgrid=True,  
            gridcolor="lightgray",  # Light grid lines for radar effect
            tickcolor="lightgray",  # Light tick color for radar effect
            tickfont=dict(size=16, color="white")  # White font for radar effect
        )
    ),
    showlegend=True
)




#fig.show()




html_template = """
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <style>
        body {{
            font-family: Arial, sans-serif;
        }}
        #hover-card {{
            display: none;
            position: fixed;
            max-width: 400px;
            width: auto;
            padding: 10px;
            background-color: #f9f9f9;
            border: 1px solid #ccc;
            box-shadow: 2px 2px 12px rgba(0, 0, 0, 0.2);
            box-sizing: border-box;
            white-space: normal;
            z-index: 1000;
        }}
        
    </style>
</head>
<body>
    {plotly_figure}
    <div id="hover-card"></div>

    <script>
    document.addEventListener('DOMContentLoaded', function() {{
    var graphDiv = document.querySelector('.js-plotly-plot');
    var hoverCard = document.getElementById('hover-card');
    var hideTimeout;
    var isHoveringOverCard = false;
    var isCardLocked = false;
    var wasDataClicked = false;

    function displayCard(data) {{
        clearTimeout(hideTimeout);

        var point = data.points[0];
        hoverCard.innerHTML = point.customdata;
        hoverCard.style.display = 'block';

        var cardHeight = hoverCard.offsetHeight;
        var topPosition = data.event.clientY - cardHeight - 10;

        if(data.event.clientY - cardHeight < 0) {{
            topPosition = data.event.clientY + 10;
        }}

        hoverCard.style.left = data.event.clientX + 'px';
        hoverCard.style.top = topPosition + 'px';
    }}

    graphDiv.on('plotly_hover', function(data) {{
        if (!isCardLocked) {{
            displayCard(data);
        }}
    }});

    graphDiv.on('plotly_click', function(data) {{
        wasDataClicked = true;
        isCardLocked = true;
        displayCard(data);
    }});

    graphDiv.on('plotly_unhover', function() {{
        if (!isCardLocked) {{
            hideTimeout = setTimeout(function() {{
                if (!isHoveringOverCard) {{
                    hoverCard.style.display = 'none';
                }}
            }}, 500);
        }}
    }});

    hoverCard.addEventListener('mouseenter', function() {{
        isHoveringOverCard = true;
        clearTimeout(hideTimeout);
    }});

    hoverCard.addEventListener('mouseleave', function() {{
        isHoveringOverCard = false;
        if (!isCardLocked) {{
            hoverCard.style.display = 'none';
        }}
    }});

    document.addEventListener('click', function(event) {{
        if (wasDataClicked) {{
            wasDataClicked = false;
            return;
        }}

        if (!hoverCard.contains(event.target)) {{
            hoverCard.style.display = 'none';
            isCardLocked = false;
        }}
    }});
}});




    </script>
</body>
</html>
"""

final_html = html_template.format(plotly_figure=fig.to_html(full_html=False))

# Save the final HTML to a file
with open("tech_radar.html", "w") as f:
    f.write(final_html)
