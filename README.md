[![pipeline status](https://gitlab.com/fdda1/technology-radar/badges/master/pipeline.svg)](https://gitlab.com/fdda1/technology-radar/-/commits/master)
[![Latest Release](https://gitlab.com/fdda1/technology-radar/-/badges/release.svg)](https://gitlab.com/fdda1/technology-radar/-/releases)

# Technology Radar Visualization for FAIR Data Spaces Project

This repository offers an interactive visualization of the technologies utilized in the [FAIR Data Spaces Project](https://www.nfdi.de/fair-data-spaces/?lang=en) aptly named "Technology Radar." It acts as a comprehensive guide, detailing various technologies, tools, platforms, and languages either in use or under evaluation. 

- **Live Radar**: View the radar [here](https://fdda1.gitlab.io/technology-radar/).
- **Data Source**: The underlying data for the radar comes from [this google sheet](https://docs.google.com/spreadsheets/d/1BbnaOa30h8ymDkl5CL8OGLUkw4wzl2FqpdkyS2PKpps/edit#gid=0).
- **Automatic Updates**: The radar is set to refresh its content every 24 hours by rechecking the source.

## Overview

At its core, the Technology Radar is an insightful visualization where users can hover over dots (representing individual technologies) to retrieve more granular information about each.

![Technology Radar Screenshot](https://gitlab.com/fdda1/technology-radar/-/raw/master/screenshot.png)  

## Key Features

- **Interactive Hover Experience**: Each dot on the radar, representing a technology, offers a detailed hover description for user clarity.
- **Minimalistic Aesthetics**: A clutter-free and streamlined design ensures that users can focus solely on the data.

## Embedding the Radar

For those wishing to incorporate the radar into another platform (like WordPress), the following iframe code can be employed:

```html
<iframe src="https://fdda1.gitlab.io/technology-radar/" width="100%" height="600" frameborder="0"></iframe>
```

## Contributing
Community input is always welcome! Whether it's contributions, feedback, or issue reports, your insights can help enhance this project. Feel free to browse the current [issues](https://gitlab.com/fdda1/technology-radar/-/boards) or initiate a new one.


## License

This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details.
